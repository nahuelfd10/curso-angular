import { Component } from '@angular/core';

interface Personaje {
  nombre: string;
  poder:number;
}

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html'
})
export class MainPageComponent  {

  nuevo = {
   nombre: 'Truncks',
   poder: 14000
 }

  agregar(): any {
   console.log ( this.nuevo);
 }



}
